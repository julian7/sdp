'use strict';

module.exports = function(environment) {
  let ENV = {
    modulePrefix: 'sourdoughplanner',
    environment
  };

  return ENV;
};
