import round from './helper';

const { module, test } = QUnit;

module('Helper: round', function(hooks) {
  test('it computes', function(assert) {
    assert.equal(round(5), 5);
  });
});
